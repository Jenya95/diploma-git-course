package sanevich.diploma.DAO;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import sanevich.diploma.DAO.basic.GroupDAO;
import sanevich.diploma.DAO.basic.UserDAO;
import sanevich.diploma.model.basic.Group;
import sanevich.diploma.model.basic.User;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class UserDAOImplTest {
    @Autowired
    GroupDAO groupDAO;

    @Autowired
    UserDAO userDAO;

    @Test
    public void addUser() {

        Group group = new Group();
        group.setName("TestGr1");
        groupDAO.save(group);

        User user1 = new User();
        user1.setGroup(groupDAO.findByName("TestGr1"));
        user1.setLastName("Test1LN");
        user1.setFirstName("Test1FN");
        user1.setPassword("Pass1");
        user1.setAge(76);
        user1.setEmail("test1@test1.test");
        userDAO.save(user1);

        User userfinded = (User) userDAO.findByEmail("test1@test1.test");
        assertEquals("User finded",userfinded.getFirstName(),"Test1FN");

        userDAO.delete(userfinded.getId());

        Group group2delete = groupDAO.findByName("TestGr1");
        groupDAO.delete(group2delete);
    }

    @Test
    public void findAllUserTest() {

        Group group = new Group();
        group.setName("TestGr2");
        groupDAO.save(group);

        User user1 = new User();
        user1.setGroup(groupDAO.findByName("TestGr2"));
        user1.setLastName("Test1LN");
        user1.setFirstName("Test1FN");
        user1.setPassword("Pass1");
        user1.setAge(76);
        user1.setEmail("test2@test1.test");
        userDAO.save(user1);

        User user2 = new User();
        user2.setGroup(groupDAO.findByName("TestGr2"));
        user2.setLastName("Test2LN");
        user2.setFirstName("Test2FN");
        user2.setPassword("Pass2");
        user2.setAge(76);
        user2.setEmail("test3@test2.test");
        userDAO.save(user2);

        int allUsers = userDAO.findAll().size();
        assertNotEquals(allUsers, 0);

        userDAO.delete(user1.getId());
        userDAO.delete(user2.getId());

        Group group2delete = groupDAO.findByName("TestGr2");
        groupDAO.delete(group2delete);
    }

    @Test
    public void findByGroupIdUserTest() {

        Group group = new Group();
        group.setName("TestGr3");
        groupDAO.save(group);

        User user1 = new User();
        user1.setGroup(groupDAO.findByName("TestGr3"));
        user1.setLastName("Test1LN");
        user1.setFirstName("Test1FN");
        user1.setPassword("Pass1");
        user1.setAge(76);
        user1.setEmail("test4@test1.test");
        userDAO.save(user1);

        Group groupFinded = groupDAO.findByName("TestGr3");
        User userfinded = (User) userDAO.findByGroupId(groupFinded.getId()).get(0);
        assertEquals("User finded",userfinded.getFirstName(),"Test1FN");

        userDAO.delete(userfinded.getId());

        Group group2delete = groupDAO.findByName("TestGr3");
        groupDAO.delete(group2delete);
    }

}