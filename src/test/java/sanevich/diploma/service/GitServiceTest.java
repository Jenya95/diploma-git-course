package sanevich.diploma.service;

import org.junit.*;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import sanevich.diploma.DAO.basic.CourseDAO;
import sanevich.diploma.DAO.basic.UserDAO;
import sanevich.diploma.model.basic.Course;
import sanevich.diploma.model.basic.User;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class GitServiceTest {

    @Autowired
    GitService gitService;

    @Autowired
    UserDAO users;

    @Autowired
    CourseDAO courses;

    @Before
    @Ignore
    public void createUser() {
        if (users.findByEmail("gipahop@p33.org") == null) {
            User user = new User();
            user.setEmail("gipahop@p33.org");
            user.setPassword("gipahopgipahop");
            users.save(user);
        }
    }

    @Test
    @Ignore
    public void addUserToGitLab() {
        User user = users.findByEmail("gipahop@p33.org");
        gitService.addUserToGitLab(user);
        Assert.assertTrue(user.getGitAccessToken() != null);
    }

    @Test
    @Ignore
    public void handleProject() {
        User user = users.findByEmail("gipahop@p33.org");
        Course course = courses.findByUniqueCode("1d74c45e-3d44-4140-bee9-b69edc189940");
        long forkedProjectId = gitService.forkProject(user,course);
        gitService.editProject(user, forkedProjectId);
        gitService.reduceUserAccessRightsToDeveloper(user, forkedProjectId);
        gitService.addTeacherToForkedProject(forkedProjectId);
    }

    @After
    @Ignore
    public void removeUser() {
        User found = users.findByEmail("gipahop@p33.org");
        if (found.getGitAccessToken() == null) {
            users.delete(found.getId());
        }
    }
}