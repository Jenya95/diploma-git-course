package sanevich.diploma.storage;

import org.springframework.core.io.Resource;
import org.springframework.web.multipart.MultipartFile;
import sanevich.diploma.model.basic.Course;

import java.nio.file.Path;
import java.util.stream.Stream;

public interface StorageService {

    void init();

    void store(MultipartFile file, Course course);

    Stream<Path> loadAll();

    Stream<Path> loadByCourse(Course course);

    Path load(String filename);

    Resource loadAsResource(String filename);

    void deleteAll();

}
