package sanevich.diploma.storage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.util.FileSystemUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;
import sanevich.diploma.DAO.basic.StorageDAO;
import sanevich.diploma.model.basic.Attachment;
import sanevich.diploma.model.basic.Course;

import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class FileSystemStorageService implements StorageService {

    private final Path rootLocation;
    private final StorageDAO attachments;
    private final Logger log = LoggerFactory.getLogger(this.getClass());


    @Autowired
    public FileSystemStorageService(StorageProperties properties, StorageDAO attachments) {
        this.rootLocation = Paths.get(properties.getLocation());
        this.attachments = attachments;
    }

    @Override
    public void store(MultipartFile file, Course course) {
        Attachment attachment;
        String filename = StringUtils.cleanPath(file.getOriginalFilename());
        try {
            if (file.isEmpty()) {
                throw new StorageException("Failed to store empty file " + filename);
            }
            if (filename.contains("..")) {
                // This is a security check
                throw new StorageException(
                        "Cannot store file with relative path outside current directory "
                                + filename);
            }
            Files.copy(file.getInputStream(), this.rootLocation.resolve(filename),
                    StandardCopyOption.REPLACE_EXISTING);

            attachment = Attachment.builder()
                    .fileName(filename)
                    .course(course)
                    .isActive(true)
                    .build();

            attachments.save(attachment);

            log.info("File {} for course {} was successfully stored", filename, course.getName());
        } catch (IOException e) {
            throw new StorageException("Failed to store file " + filename, e);
        }
    }

    @Override
    public Stream<Path> loadAll() {
        try {
            return Files.walk(this.rootLocation, 1)
                    .filter(path -> !path.equals(this.rootLocation))
                    .map(this.rootLocation::relativize);
        } catch (IOException e) {
            throw new StorageException("Failed to read stored files", e);
        }

    }

    @Override
    public Stream<Path> loadByCourse(Course course) {

        Stream<Path> allFiles = loadAll();
        List<String> fileNames = attachments.findByCourseId(course.getId())
                .stream()
                .map(Attachment::getFileName)
                .collect(Collectors.toList());

        return allFiles
                .filter(x -> fileNames.contains(x.getFileName().toString()));


    }

    @Override
    public Path load(String filename) {
        return rootLocation.resolve(filename);
    }

    @Override
    public Resource loadAsResource(String filename) {
        try {
            Path file = load(filename);
            Resource resource = new UrlResource(file.toUri());
            if (resource.exists() || resource.isReadable()) {
                return resource;
            } else {
                throw new StorageFileNotFoundException(
                        "Could not read file: " + filename);

            }
        } catch (MalformedURLException e) {
            throw new StorageFileNotFoundException("Could not read file: " + filename, e);
        }
    }

    @Override
    public void deleteAll() {
        FileSystemUtils.deleteRecursively(rootLocation.toFile());
    }

    @Override
    public void init() {
        try {
            Files.createDirectories(rootLocation);
        } catch (IOException e) {
            throw new StorageException("Could not initialize storage", e);
        }
    }
}
