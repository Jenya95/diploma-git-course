package sanevich.diploma;

import org.apache.commons.dbcp.BasicDataSource;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.*;
import org.springframework.core.env.Environment;
import org.springframework.orm.hibernate5.HibernateTransactionManager;
import org.springframework.orm.hibernate5.LocalSessionFactoryBuilder;
import sanevich.diploma.model.archive.DwhCourse;
import sanevich.diploma.model.archive.DwhLesson;
import sanevich.diploma.model.basic.*;
import sanevich.diploma.model.basic.Role;

import javax.sql.DataSource;
import java.util.Properties;

@Configuration
@ComponentScan
@PropertySource("classpath:database.properties")
public class DataBaseConfiguration {

    private final Environment env;

    @Autowired
    public DataBaseConfiguration(Environment env) {
        this.env = env;
    }

    @Autowired
    @Bean
    @Primary
    public HibernateTransactionManager transactionManager(SessionFactory sessionFactory) {
        HibernateTransactionManager hibernateTransactionManager = new HibernateTransactionManager();
        hibernateTransactionManager.setSessionFactory(sessionFactory);
        return hibernateTransactionManager;
    }

    /**
     * Значения проепертей хранятся в файле database.properties
     *
     * @return basicDataSource
     */
    @Bean(name = "basicDataSource")
    @Primary
    public BasicDataSource basicDataSource() {
        BasicDataSource basicDataSource = new BasicDataSource();
        basicDataSource.setDriverClassName(env.getProperty("db.driverClassName"));
        basicDataSource.setUrl(env.getProperty("db.connectString"));
        basicDataSource.setUsername(env.getProperty("db.username"));
        basicDataSource.setPassword(env.getProperty("db.password"));
        return basicDataSource;
    }


    @Autowired
    @Bean(name = "basicSessionFactory")
    @Primary
    public SessionFactory getSessionFactory(@Qualifier("basicDataSource") DataSource dataSource) {
        LocalSessionFactoryBuilder sessionBuilder = new LocalSessionFactoryBuilder(dataSource);
        sessionBuilder.addAnnotatedClasses(User.class);
        sessionBuilder.addAnnotatedClasses(Role.class);
        sessionBuilder.addAnnotatedClasses(Group.class);
        sessionBuilder.addAnnotatedClasses(Course.class);
        sessionBuilder.addAnnotatedClasses(Lesson.class);
        sessionBuilder.addAnnotatedClasses(Attachment.class);
        sessionBuilder.addProperties(hibernateProperties());
        return sessionBuilder.buildSessionFactory();
    }

    @Autowired
    @Bean(name = "dwhTransactionManager")
    public HibernateTransactionManager dwhTransactionManager(@Qualifier("dwhSessionFactory") SessionFactory sessionFactory) {
        HibernateTransactionManager hibernateTransactionManager = new HibernateTransactionManager();
        hibernateTransactionManager.setSessionFactory(sessionFactory);
        return hibernateTransactionManager;
    }

    /**
     * Значения проепертей хранятся в файле database.properties
     *
     * @return dwhDataSource
     */
    @Bean(name = "dwhDataSource")
    public BasicDataSource dwhDataSource() {
        BasicDataSource basicDataSource = new BasicDataSource();
        basicDataSource.setDriverClassName(env.getProperty("dwh.db.driverClassName"));
        basicDataSource.setUrl(env.getProperty("dwh.db.connectString"));
        basicDataSource.setUsername(env.getProperty("dwh.db.username"));
        basicDataSource.setPassword(env.getProperty("dwh.db.password"));
        return basicDataSource;
    }

    @Autowired
    @Bean(name = "dwhSessionFactory")
    public SessionFactory getDwhSessionFactory(@Qualifier("dwhDataSource") DataSource dataSource) {
        LocalSessionFactoryBuilder sessionBuilder = new LocalSessionFactoryBuilder(dataSource);
        sessionBuilder.addAnnotatedClasses(DwhCourse.class);
        sessionBuilder.addAnnotatedClasses(DwhLesson.class);
        sessionBuilder.addProperties(hibernateProperties());
        return sessionBuilder.buildSessionFactory();
    }

    private Properties hibernateProperties() {
        return new Properties() {
            {
                setProperty("hibernate.dialect", "org.hibernate.dialect.MySQL5Dialect");
                //setProperty("hibernate.show_sql", "true");
                setProperty("hibernate.hbm2ddl.auto", "update");
                setProperty("hibernate.enable_lazy_load_no_trans", "true");
                setProperty("hibernate.format_sql", "true");
            }
        };
    }
}
