package sanevich.diploma;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import sanevich.diploma.storage.StorageService;


@SpringBootApplication(exclude = HibernateJpaAutoConfiguration.class)
@EnableCaching
public class GitCourseApplication {

	public static void main(String[] args) {
		SpringApplication.run(GitCourseApplication.class, args);
	}

    @Bean
    CommandLineRunner init(StorageService storageService) {
        return (args) -> {
            storageService.init();
        };
    }

}
