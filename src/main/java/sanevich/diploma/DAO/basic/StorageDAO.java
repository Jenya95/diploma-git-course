package sanevich.diploma.DAO.basic;

import sanevich.diploma.model.basic.Attachment;

import java.util.List;

public interface StorageDAO {
    void save(Attachment attachment);
    List<Attachment> findByCourseId(long courseId);
    List<Attachment> findAll();
}
