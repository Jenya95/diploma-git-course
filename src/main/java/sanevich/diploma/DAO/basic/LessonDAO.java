package sanevich.diploma.DAO.basic;

import sanevich.diploma.model.basic.Course;
import sanevich.diploma.model.basic.Lesson;

import java.util.List;

public interface LessonDAO {
    Lesson findById(long id);
    List <Lesson> findByCourse(Course course);
    List <Lesson> findAll();
    void save(Lesson lesson);
}
