package sanevich.diploma.DAO.basic;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import sanevich.diploma.model.basic.Group;

import java.util.List;

@Repository
@Transactional
@SuppressWarnings("unchecked")
public class GroupDAOImpl implements GroupDAO{

    private final SessionFactory sessionFactory;

    @Autowired
    public GroupDAOImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }


    public List <Group> findAll() {
        Query query = sessionFactory.getCurrentSession().createQuery("from Group ");
        return (List <Group>) query.list();
    }

    public Group findByName(String name) {
        Query query = sessionFactory.getCurrentSession().createQuery("from Group where name=:name");
        query.setParameter("name",name);
        return (Group) query.uniqueResult();
    }

    @Override
    public Group findById(long id) {
        Query query = sessionFactory.getCurrentSession().createQuery("from Group where id=:id");
        query.setParameter("id",id);
        return (Group) query.uniqueResult();
    }

    @Override
    public void save(Group group) {
        Session session = this.sessionFactory.getCurrentSession();
        session.saveOrUpdate(group);
    }

    @Override
    public void update(Group group) {
        Session session = this.sessionFactory.getCurrentSession();
        session.update(group);
    }

    @Override
    public void delete(Group group) {
        Session session = this.sessionFactory.getCurrentSession();
        session.delete(group);
    }
}
