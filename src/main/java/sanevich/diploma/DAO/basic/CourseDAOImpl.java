package sanevich.diploma.DAO.basic;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import sanevich.diploma.model.basic.Course;
import sanevich.diploma.model.basic.User;

import java.util.List;

@Repository
@Transactional
@SuppressWarnings("unchecked")
public class CourseDAOImpl implements CourseDAO {

    private final SessionFactory sessionFactory;

    @Autowired
    public CourseDAOImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List <Course> findAll() {
        Query query = sessionFactory.getCurrentSession().createQuery("from Course order by dateBegin desc");
        return (List <Course>) query.list();
    }

    @Override
    public Course findByName(String name) {
        Query query = sessionFactory.getCurrentSession().createQuery("from Course where name=:name");
        query.setParameter("name", name);
        return (Course) query.uniqueResult();
    }

    @Override
    @Cacheable("courses")
    public Course findById(long id) {
        Query query = sessionFactory.getCurrentSession().createQuery("from Course where id=:id");
        query.setParameter("id", id);
        return (Course) query.uniqueResult();
    }

    @Override
    @Cacheable("courses")
    public List <Course> findByUser(User user) {
        Query query = sessionFactory.getCurrentSession().createQuery("select c from Course c join c.users g where g.id=:id ");
        query.setParameter("id", user.getId());
        return (List <Course>) query.list();
    }

    @Override
    public Course findByUniqueCode(String uniqueCode) {
        Query query = sessionFactory.getCurrentSession().createQuery("from Course where uniqueCode=:uniqueCode");
        query.setParameter("uniqueCode", uniqueCode);
        return (Course) query.uniqueResult();
    }

    @Override
    @CacheEvict(value = "courses", allEntries=true)
    public void save(Course course) {
        Session session = this.sessionFactory.getCurrentSession();
        session.saveOrUpdate(course);
    }

    @Override
    public void delete(Course course) {
        Session session = this.sessionFactory.getCurrentSession();
        if (course != null) {
            session.delete(course);
        }
    }
}
