package sanevich.diploma.DAO.basic;

import sanevich.diploma.model.basic.Course;
import sanevich.diploma.model.basic.User;

import java.util.List;

public interface CourseDAO {
    List <Course> findAll();
    Course findByName(String name);
    Course findById(long id);
    List <Course> findByUser(User user);
    Course findByUniqueCode(String uniqueCode);
    void save(Course course);
    void delete(Course course);
}
