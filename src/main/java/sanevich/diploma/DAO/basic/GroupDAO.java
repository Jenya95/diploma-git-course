package sanevich.diploma.DAO.basic;

import sanevich.diploma.model.basic.Group;

import java.util.List;

public interface GroupDAO {
    List <Group> findAll();
    Group findByName(String name);
    Group findById(long id);
    void save(Group group);
    void update(Group group);
    void delete(Group group);
}
