package sanevich.diploma.DAO.basic;

import sanevich.diploma.model.basic.User;

import java.util.List;

/**
 * Created by esanevich on 02.07.17.
 */
public interface UserDAO {
    User findByEmail(String email);

    List <User> findAll();

    void save(User user);

    User findById(long id);

    void delete(long id);

    void updateUser(User user);

    List findByGroupId(long groupId);
}
