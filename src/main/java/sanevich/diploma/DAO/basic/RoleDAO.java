package sanevich.diploma.DAO.basic;

import sanevich.diploma.model.basic.Role;

public interface RoleDAO {
    Role findByName(String name);
    Role findById(long id);
}
