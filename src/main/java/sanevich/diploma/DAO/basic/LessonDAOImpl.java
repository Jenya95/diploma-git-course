package sanevich.diploma.DAO.basic;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import sanevich.diploma.model.basic.Course;
import sanevich.diploma.model.basic.Lesson;

import java.util.List;

@Repository
@Transactional
@SuppressWarnings("unchecked")
public class LessonDAOImpl implements LessonDAO {
    private final SessionFactory sessionFactory;

    @Autowired
    public LessonDAOImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    @Cacheable("lessons")
    public Lesson findById(long id) {
        Query query = sessionFactory.getCurrentSession().createQuery("from Lesson where id=:id");
        query.setParameter("id",id);
        return (Lesson) query.uniqueResult();
    }

    @Override
    @Cacheable("lessons")
    public List <Lesson> findByCourse(Course course) {
        Query query = sessionFactory.getCurrentSession().createQuery("from Lesson where course.id=:id");
        query.setParameter("id", course.getId());
        return (List <Lesson>) query.list();
    }

    @Override
    public List <Lesson> findAll() {
        Query query = sessionFactory.getCurrentSession().createQuery("from Lesson");
        return (List <Lesson>) query.list();
    }

    @Override
    @CacheEvict(value = "lessons", allEntries=true)
    public void save(Lesson lesson) {
        Session session = this.sessionFactory.getCurrentSession();
        session.saveOrUpdate(lesson);
    }
}
