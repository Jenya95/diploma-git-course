package sanevich.diploma.DAO.basic;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import sanevich.diploma.model.basic.Attachment;

import java.util.List;

@Repository
@Transactional
@SuppressWarnings("unchecked")
public class StorageDAOImpl implements StorageDAO {

    private final SessionFactory sessionFactory;

    @Autowired
    public StorageDAOImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public void save(Attachment attachment) {
        Session session = this.sessionFactory.getCurrentSession();
        session.saveOrUpdate(attachment);
    }

    @Override
    public List<Attachment> findByCourseId(long courseId) {
        Query query = sessionFactory.getCurrentSession().createQuery("from Attachment where course.id=:id");
        query.setParameter("id", courseId);
        return (List <Attachment>) query.list();
    }

    @Override
    public List<Attachment> findAll() {
        Query query = sessionFactory.getCurrentSession().createQuery("from Attachment");
        return (List <Attachment>) query.list();
    }
}
