package sanevich.diploma.DAO.basic;

import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import sanevich.diploma.model.basic.Role;

@Repository
@Transactional
@SuppressWarnings("unchecked")
public class RoleDAOImpl implements RoleDAO {

    private final SessionFactory sessionFactory;

    @Autowired
    public RoleDAOImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public Role findByName(String name) {
        Query query = sessionFactory.getCurrentSession().createQuery("from Role where role=:role");
        query.setParameter("role",name);
        return (Role) query.uniqueResult();
    }

    @Override
    public Role findById(long id) {
        Query query = sessionFactory.getCurrentSession().createQuery("from Role where id=:id");
        query.setParameter("id",id);
        return (Role) query.uniqueResult();
    }
}
