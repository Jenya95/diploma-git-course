package sanevich.diploma.DAO.basic;


import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import sanevich.diploma.model.basic.User;

import java.util.List;

@Repository
@Transactional
@SuppressWarnings("unchecked")
public class UserDAOImpl implements UserDAO {
    private final SessionFactory sessionFactory;

    @Autowired
    public UserDAOImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    @Cacheable("users")
    public User findByEmail(String email) {

        List <User> users;

        users = sessionFactory.getCurrentSession()
                .createQuery("from User where email=:email")
                .setParameter("email", email)
                .list();

        if (users.size() > 0) {
            return users.get(0);
        } else {
            return null;
        }

    }

    @Override
    public List <User> findAll() {
        List <User> users;

        users = sessionFactory.getCurrentSession()
                .createQuery("from User")
                .list();

        return users;
    }

    @Override
    @CacheEvict(value = {"users","courses"}, allEntries=true)
    public void save(User user) {
        Session session = this.sessionFactory.getCurrentSession();
        session.saveOrUpdate(user);
    }

    @Override
    @Cacheable("users")
    public User findById(long id) {
        Query query = sessionFactory.getCurrentSession().createQuery("from User where id=:id");
        query.setParameter("id", id);
        return (User) query.uniqueResult();
    }

    @Override
    public void delete(long id) {
        Session session = this.sessionFactory.getCurrentSession();
        User user = session.load(User.class, id);
        if (null != user) {
            session.delete(user);
        }
    }

    @Override
    @CacheEvict(value = "users", allEntries=true)
    public void updateUser(User user) {
        Session session = this.sessionFactory.getCurrentSession();
        session.update(user);
    }

    @Override
    public List <User> findByGroupId(long groupId) {
        List <User> users;

        users = sessionFactory.getCurrentSession()
                .createQuery("from User where group.id=:groupId")
                .setParameter("groupId", groupId)
                .list();

        return users;
    }

}
