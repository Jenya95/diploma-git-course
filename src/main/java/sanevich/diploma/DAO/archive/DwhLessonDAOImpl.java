package sanevich.diploma.DAO.archive;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import sanevich.diploma.model.archive.DwhLesson;

import java.util.List;

@Repository
@Transactional("dwhTransactionManager")
@SuppressWarnings("unchecked")
public class DwhLessonDAOImpl implements DwhLessonDAO{

    private final SessionFactory sessionFactory;

    @Autowired
    public DwhLessonDAOImpl(@Qualifier("dwhSessionFactory") SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List <DwhLesson> findAll() {
        Query query = sessionFactory.getCurrentSession().createQuery("from DwhLesson");
        return (List <DwhLesson>) query.list();
    }

    @Override
    public void save(DwhLesson dwhLesson) {
        Session session = this.sessionFactory.getCurrentSession();
        session.saveOrUpdate(dwhLesson);
    }

    @Override
    public DwhLesson findById(long id) {
        Query query = sessionFactory.getCurrentSession().createQuery("from DwhLesson where id=:id");
        query.setParameter("id", id);
        return (DwhLesson) query.uniqueResult();
    }
}
