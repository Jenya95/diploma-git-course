package sanevich.diploma.DAO.archive;

import sanevich.diploma.model.archive.DwhCourse;

import java.util.List;

public interface DwhCourseDAO {
    List <DwhCourse> findAll();
    void save(DwhCourse dwhCourse);
    DwhCourse findById(long id);
    void delete(DwhCourse course);
}
