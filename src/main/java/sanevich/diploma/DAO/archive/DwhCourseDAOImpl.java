package sanevich.diploma.DAO.archive;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import sanevich.diploma.model.archive.DwhCourse;

import java.util.List;

@Repository
@Transactional("dwhTransactionManager")
@SuppressWarnings("unchecked")
public class DwhCourseDAOImpl implements DwhCourseDAO{

    private final SessionFactory sessionFactory;

    @Autowired
    public DwhCourseDAOImpl(@Qualifier("dwhSessionFactory") SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List <DwhCourse> findAll() {
        Query query = sessionFactory.getCurrentSession().createQuery("from DwhCourse");
        return (List <DwhCourse>) query.list();
    }

    @Override
    public void save(DwhCourse dwhCourse) {
        Session session = this.sessionFactory.getCurrentSession();
        session.saveOrUpdate(dwhCourse);
    }

    @Override
    public DwhCourse findById(long id) {
        Query query = sessionFactory.getCurrentSession().createQuery("from DwhCourse where id=:id");
        query.setParameter("id", id);
        return (DwhCourse) query.uniqueResult();
    }

    @Override
    public void delete(DwhCourse course) {
        Session session = this.sessionFactory.getCurrentSession();
        if (course != null) {
            session.delete(course);
        }
    }
}
