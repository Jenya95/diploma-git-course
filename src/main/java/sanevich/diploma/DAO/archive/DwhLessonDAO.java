package sanevich.diploma.DAO.archive;

import sanevich.diploma.model.archive.DwhLesson;

import java.util.List;

public interface DwhLessonDAO {
    List <DwhLesson> findAll();
    void save(DwhLesson dwhLesson);
    DwhLesson findById(long id);
}
