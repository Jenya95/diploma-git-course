package sanevich.diploma.model.basic;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Tolerate;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.Set;

@Entity
@Table(name = "m_course")
@Getter
@Setter
@Builder
public class Course {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private long id;

    @Column
    private Boolean isActive;

    @Column
    @CreationTimestamp
    private LocalDateTime created;

    @Column
    @UpdateTimestamp
    private LocalDateTime updated;

    private String name;

    @Column
    @DateTimeFormat(pattern = "dd.MM.yyyy")
    private Date dateBegin;

    @Column
    @DateTimeFormat(pattern = "dd.MM.yyyy")
    private Date dateEnd;

    @Column
    private String uniqueCode;

    @Lob
    @Column
    private String description;

    @Column(length = 1000)
    private String originLink;

    @Column(length = 1000)
    private String origin;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "course")
    private Set<Lesson> lessons;

    @ManyToMany(mappedBy = "courses")
    private Set<User> users;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "course")
    private Set<Attachment> attachments;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Course course = (Course) o;

        if (id != course.id) return false;
        return name.equals(course.name);
    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = name != null ? 31 * result + name.hashCode() : 31 * result;
        return result;
    }

    @Tolerate
    public Course() {
    }
}
