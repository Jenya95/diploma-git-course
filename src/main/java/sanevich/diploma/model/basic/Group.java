package sanevich.diploma.model.basic;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.hibernate.validator.constraints.NotBlank;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Set;

/**
 * Created by esanevich on 11.07.17.
 */
@Entity
@Table(name="m_group")
@Getter
@Setter
public class Group {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private long id;

    @Column
    @CreationTimestamp
    private LocalDateTime created;

    @Column
    @UpdateTimestamp
    private LocalDateTime updated;

    @Column(unique = true)
    @NotBlank
    private String name;

    @OneToMany(cascade=CascadeType.ALL, mappedBy="group")
    private Set <User> students;
}
