package sanevich.diploma.model.basic;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Tolerate;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.hibernate.validator.constraints.NotBlank;
import sanevich.diploma.service.MarkdownConverter;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name="m_lesson")
@Getter
@Setter
@Builder
public class Lesson {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Column
    private Boolean isActive;

    @Column
    @CreationTimestamp
    private LocalDateTime created;

    @Column
    @UpdateTimestamp
    private LocalDateTime updated;

    @ManyToOne
    @JoinColumn(name="course_id")
    private Course course;

    @Lob
    @Column()
    private String fullPostText;

    @Column()
    @NotBlank
    private String name;

    @Column
    private String description;

    @Tolerate
    public Lesson (Course course) {
        this.course = course;
    }

    @Tolerate
    public Lesson() {}

    public static String shortPartSeparator() {
        return "===cut===";
    }

    public String fullPostTextHtml() {
        return MarkdownConverter.toHtml(getFullPostText().replace(shortPartSeparator(), ""));
    }
}
