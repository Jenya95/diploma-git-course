package sanevich.diploma.model.archive;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Tolerate;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Set;

@Entity
@Table(name="arch_course")
@Getter
@Setter
@Builder
public class DwhCourse {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Column
    @CreationTimestamp
    private LocalDateTime created;

    @Column
    @UpdateTimestamp
    private LocalDateTime updated;

    private String name;

    @Lob
    @Column
    private String description;

    @OneToMany(cascade=CascadeType.ALL, mappedBy="dwhCourse")
    private Set <DwhLesson> lessons;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DwhCourse course = (DwhCourse) o;

        if (id != course.id) return false;
        return name.equals(course.name);
    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = name != null ? 31 * result + name.hashCode() : 31 * result;
        return result;
    }

    @Tolerate
    public DwhCourse() {}
}
