package sanevich.diploma.model.archive;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Tolerate;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.hibernate.validator.constraints.NotBlank;
import sanevich.diploma.service.MarkdownConverter;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name="arch_lesson")
@Getter
@Setter
@Builder
public class DwhLesson {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Column
    @CreationTimestamp
    private LocalDateTime created;

    @Column
    @UpdateTimestamp
    private LocalDateTime updated;

    @ManyToOne
    private DwhCourse dwhCourse;

    @Lob
    @Column
    private String fullPostText;

    @Column
    @NotBlank
    private String name;

    @Column
    private String description;

    public static String shortPartSeparator() {
        return "===cut===";
    }

    public String fullPostTextHtml() {
        return MarkdownConverter.toHtml(getFullPostText().replace(shortPartSeparator(), ""));
    }

    @Tolerate
    public DwhLesson() {}
}
