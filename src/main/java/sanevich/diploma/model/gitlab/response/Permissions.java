
package sanevich.diploma.model.gitlab.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "project_access",
    "group_access"
})
public class Permissions {

    @JsonProperty("project_access")
    private ProjectAccess projectAccess;
    @JsonProperty("group_access")
    private Object groupAccess;

    @JsonProperty("project_access")
    public ProjectAccess getProjectAccess() {
        return projectAccess;
    }

    @JsonProperty("project_access")
    public void setProjectAccess(ProjectAccess projectAccess) {
        this.projectAccess = projectAccess;
    }

    @JsonProperty("group_access")
    public Object getGroupAccess() {
        return groupAccess;
    }

    @JsonProperty("group_access")
    public void setGroupAccess(Object groupAccess) {
        this.groupAccess = groupAccess;
    }
}
