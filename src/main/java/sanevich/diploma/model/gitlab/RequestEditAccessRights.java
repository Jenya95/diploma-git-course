package sanevich.diploma.model.gitlab;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.ToString;

@Builder
@ToString
public class RequestEditAccessRights {
    private final long id;
    @JsonProperty(value = "user_id")
    private final long userId;
    @JsonProperty(value = "access_level")
    private final long accessLevel;
}
