package sanevich.diploma.model.gitlab;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Getter;
import lombok.ToString;

@Builder
@Getter
@ToString
public class RequestAddUser {
    private final String email;
    private final String password;
    private final String username;
    private final String name;
    @JsonProperty(value = "projects_limit")
    private final int projectsLimit;
    @JsonProperty(value = "can_create_group")
    private final boolean canCreateGroup;
}
