package sanevich.diploma.model.gitlab;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

@Builder
@Getter
@ToString
public class ResponseOAuth {
    @JsonProperty(value = "access_token")
    private final String accessToken;
    @JsonProperty(value = "refresh_token")
    private final String refreshToken;
    @JsonProperty(value = "token_type")
    private final String tokenType;
    private final String scope;
    @JsonProperty(value = "created_at")
    private final String createdAt;
}
