package sanevich.diploma.model.gitlab;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Getter;
import lombok.ToString;

@Builder
@Getter
@ToString
public class RequestOAuth {
    @JsonProperty(value = "grant_type")
    private final String grantType;
    private final String username;
    private final String password;
}
