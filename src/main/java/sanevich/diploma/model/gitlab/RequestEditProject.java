package sanevich.diploma.model.gitlab;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.ToString;

@Builder
@ToString
public class RequestEditProject {
    private final long id;

    @JsonProperty(value = "visibility_level")
    private final int visibilityLevel;

    @JsonProperty(value = "issues_enabled")
    private final boolean issuesEnabled;

    @JsonProperty(value = "merge_requests_enabled")
    private final boolean mergeRequestsEnabled;
}
