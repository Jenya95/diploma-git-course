package sanevich.diploma.security;

/**
 * Created by esanevich on 02.07.17.
 */
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.InternalAuthenticationServiceException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import sanevich.diploma.DAO.basic.UserDAO;
import sanevich.diploma.model.basic.User;

@Service("userDetailsService")
public class MyUserDetailService implements UserDetailsService {

    private final UserDAO users;

    @Autowired
    public MyUserDetailService(UserDAO users) {
        this.users = users;
    }

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        UserDetails loadedUser;

        try {
            User client = users.findByEmail(email);
            loadedUser = new org.springframework.security.core.userdetails.User(
                    client.getEmail(), client.getPassword(), client.getIsActive(), true, true, true,
                    client.getRoles());
        } catch (Exception repositoryProblem) {
            throw new InternalAuthenticationServiceException(repositoryProblem.getMessage(), repositoryProblem);
        }
        return loadedUser;
    }

}

