package sanevich.diploma.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.method.annotation.MvcUriComponentsBuilder;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import sanevich.diploma.DAO.basic.CourseDAO;
import sanevich.diploma.DAO.basic.LessonDAO;
import sanevich.diploma.DAO.basic.UserDAO;
import sanevich.diploma.model.basic.Course;
import sanevich.diploma.model.basic.Lesson;
import sanevich.diploma.model.basic.User;
import sanevich.diploma.service.GitService;
import sanevich.diploma.storage.StorageFileNotFoundException;
import sanevich.diploma.storage.StorageService;

import javax.validation.Valid;
import java.util.UUID;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/course")
@PreAuthorize("hasRole('ADMIN')")
public class LessonsAdminController {

    private final CourseDAO courses;
    private final LessonDAO lessons;
    private final UserDAO users;
    private final StorageService storageService;
    private final GitService gitService;
    private final Logger log = LoggerFactory.getLogger(this.getClass());


    @Autowired
    public LessonsAdminController(LessonDAO lessons, CourseDAO courses, UserDAO users, StorageService storageService,
                                  GitService gitService) {
        this.lessons = lessons;
        this.courses = courses;
        this.users = users;
        this.storageService = storageService;
        this.gitService = gitService;
    }

    public User getCurrentUser() {
        UserDetails user = (org.springframework.security.core.userdetails.User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return users.findByEmail(user.getUsername());
    }

    /**
     * Отображение страницы курса со списком лекций
     * @param id - id курса
     */
    @GetMapping("/{id}")
    public String showAllLessonsInCourse(Model model, @PathVariable("id") long id) {

        String baseUrl = gitService.getBaseEnvLinkURL();
        Course course = courses.findById(id);

        model.addAttribute("lessons", lessons.findByCourse(course));
        model.addAttribute("course", courses.findById(id));
        model.addAttribute("baseUrl", baseUrl);
        model.addAttribute("files", storageService.loadByCourse(course)
                .map(path ->
                        MvcUriComponentsBuilder.fromMethodName(LessonsAdminController.class,
                        "serveFile",
                        path.getFileName().toString()).build().toString())
                .collect(Collectors.toList()));
        return "teacher/courses/lessons/lessonList";
    }

    @GetMapping("/files/{filename:.+}")
    @PreAuthorize("hasAnyRole('ADMIN','STUDENT')")
    @ResponseBody
    public ResponseEntity<Resource> serveFile(@PathVariable String filename) {

        Resource file = storageService.loadAsResource(filename);
        log.info("User {} downloaded file {}", getCurrentUser().getEmail(), filename);
        return ResponseEntity.ok().header(HttpHeaders.CONTENT_DISPOSITION,
                "attachment; filename=\"" + file.getFilename() + "\"").body(file);
    }

    @PostMapping("/file/{course_id}")
    public String handleFileUpload(@RequestParam("file") MultipartFile file,
                                   RedirectAttributes redirectAttributes,
                                   @PathVariable("course_id") long courseId) {

        if (!file.isEmpty()) {
            storageService.store(file, courses.findById(courseId));
            redirectAttributes.addFlashAttribute("msg",
                    "You successfully uploaded " + file.getOriginalFilename() + "!");
            log.info("User {} successfully uploaded file {} for course {}", getCurrentUser().getEmail(), file.getOriginalFilename(), courses.findById(courseId).getName());
        }

        return "redirect:/course/" + courseId;
    }

    @ExceptionHandler(StorageFileNotFoundException.class)
    public ResponseEntity<?> handleStorageFileNotFound(StorageFileNotFoundException exc) {
        return ResponseEntity.notFound().build();
    }

    /**
     * Отображение страницы добавления лекции в курс
     * @param courseId - id курса
     */
    @GetMapping("/{id}/addLesson")
    public String addLesson(Model model, @PathVariable("id") long courseId) {
        model.addAttribute("edit", false);
        model.addAttribute("course", courses.findById(courseId));
        model.addAttribute("lesson", new Lesson(courses.findById(courseId)));
        return "teacher/courses/lessons/lessonAdd";
    }

    /**
     * Отображение страницы обновления лекции в курсе
     * @param courseId - id курса
     */
    @GetMapping("/{course_id}/updateLesson/{lesson_id}")
    public String updateLesson(Model model,
                               @PathVariable("course_id") long courseId,
                               @PathVariable("lesson_id") long lessonId) {
        model.addAttribute("edit", true);
        model.addAttribute("lesson", lessons.findById(lessonId));
        model.addAttribute("course", courses.findById(courseId));
        return "teacher/courses/lessons/lessonUpdate";
    }

    /**
     * Обработка обновления лекции
     * @param courseId - id курса
     * @param lessonId - id лекции
     */
    @PostMapping("/{course_id}/update/{lesson_id}")
    public String updateLesson(@Valid Lesson updatedLesson,
                               BindingResult bindingResult,
                               Model model,
                               @PathVariable("course_id") long courseId,
                               @PathVariable("lesson_id") long lessonId) {
        if (bindingResult.hasErrors()) {
            return addLesson(model, courseId);
        }

        Lesson lesson = lessons.findById(lessonId);
        lesson.setDescription(updatedLesson.getDescription());
        lesson.setFullPostText(updatedLesson.getFullPostText());
        lesson.setName(updatedLesson.getName());
        lessons.save(lesson);
        log.info("Lesson {} of course {} updated", lesson.getName(), lesson.getCourse().getName());
        model.addAttribute("lessons", lessons.findByCourse(courses.findById(courseId)));
        return "redirect:/course/{course_id}";
    }


    /**
     * Отображение страницы списка слушателей данного курса
     * @param courseId - id курса
     */
    @GetMapping("/{id}/studentList")
    public String showStudentsOfCourse(Model model, @PathVariable("id") long courseId) {
        model.addAttribute("course", courses.findById(courseId));
        return "teacher/courses/studentList";
    }

    /**
     * Удаление слушателя из-под курса
     * @param courseId - id курса
     * @param userId - id студента которого нужно удалить
     */
    @PostMapping("/{course_id}/studentList/{user_id}/delete")
    public String deleteUserFromCourse(Model model,
                                       @PathVariable("course_id") long courseId,
                                       @PathVariable("user_id") long userId) {
        User user = users.findById(userId);
        user.getCourses().remove(courses.findById(courseId));
        users.save(user);

        log.info("User {} excluded from course {}", user.getEmail(), courses.findById(courseId).getName());

        model.addAttribute("course", courses.findById(courseId));
        return "redirect:teacher/courses/studentList";
    }

    /**
     * Сгенерировать новоую invite ссылку для курса - ajax
     * @param courseId - id курса
     */
    @GetMapping("/{id}/generate")
    public String generateNewInviteLink(Model model, @PathVariable("id") long courseId) {

        String baseUrl = gitService.getBaseEnvLinkURL();
        Course course = courses.findById(courseId);
        UUID uuid = UUID.randomUUID();
        course.setUniqueCode(uuid.toString());
        courses.save(course);
        model.addAttribute("course", courses.findById(courseId));
        model.addAttribute("baseUrl", baseUrl);

        return "teacher/courses/lessons/lessonList :: invite-course-link";
    }

    /**
     * Обработка добавления новой лекции
     * @param courseId - id курса
     */
    @PostMapping("/{course_id}/addLesson")
    public String saveLesson(@Valid Lesson lesson,
                             BindingResult bindingResult,
                             Model model,
                             @PathVariable("course_id") long courseId) {

        if (bindingResult.hasErrors()) {
            return addLesson(model, courseId);
        }

        lesson.setCourse(courses.findById(courseId));
        lesson.setIsActive(true);
        lessons.save(lesson);

        log.info("Lesson {} added to course {}", lesson.getName(), lesson.getCourse().getName());
        model.addAttribute("lessons", lessons.findByCourse(courses.findById(courseId)));
        return "redirect:/course/{course_id}";
    }

    /**
     * Обработка активирования урока AJAX
     *
     * @param courseId - id курса
     * @param checked - требуемое состояние
     */
    @GetMapping("/{course_id}/{lesson_id}/updateActive")
    public String updateActive(Model model,
                               @PathVariable("course_id") long courseId,
                               @PathVariable("lesson_id") long lessonId,
                               @RequestParam(value = "checked", required = false)
                                       Boolean checked) {

        Lesson lesson = lessons.findById(lessonId);
        if (checked == null) {
            checked = false;
        }
        lesson.setIsActive(checked);
        lessons.save(lesson);
        model.addAttribute("course",courses.findById(courseId));
        model.addAttribute("lessons", lessons.findByCourse(courses.findById(courseId)));

        return "teacher/courses/lessons/lessonList :: lessons";
    }
}
