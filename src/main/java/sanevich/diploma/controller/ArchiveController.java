package sanevich.diploma.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import sanevich.diploma.DAO.archive.DwhCourseDAO;
import sanevich.diploma.DAO.archive.DwhLessonDAO;
import sanevich.diploma.DAO.basic.CourseDAO;
import sanevich.diploma.DAO.basic.LessonDAO;
import sanevich.diploma.model.archive.DwhCourse;
import sanevich.diploma.model.archive.DwhLesson;
import sanevich.diploma.model.basic.Course;
import sanevich.diploma.model.basic.Lesson;

import java.util.Date;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/courses")
@PreAuthorize("hasRole('ADMIN')")
public class ArchiveController {

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    private final DwhCourseDAO dwhCourses;
    private final CourseDAO courses;
    private final LessonDAO lessons;
    private final DwhLessonDAO dwhLessons;

    @Autowired
    public ArchiveController(DwhCourseDAO dwhCourses, CourseDAO courses, DwhLessonDAO dwhLessons, LessonDAO lessons) {
        this.dwhCourses = dwhCourses;
        this.courses = courses;
        this.dwhLessons = dwhLessons;
        this.lessons = lessons;
    }

    @GetMapping(value = "/archive")
    public String showArchivedCourses(Model model) {
        model.addAttribute("dwhCourses", dwhCourses.findAll());
        return "teacher/archive/archivedCourseList";
    }

    /**
     * Перенос курса в архив AJAX
     * Выполняется полность в ручном режиме - ETL
     * Сначала переносятся лекции, а затем - сам курс
     *
     * @param courseId - id курса
     */
    @GetMapping("/{course_id}/moveToArchive")
    public String etlMoveCourseToArchive(Model model,
                                         @PathVariable("course_id") long courseId) {

        Course course = courses.findById(courseId);
        Set <Lesson> lessons = course.getLessons();

        Set <DwhLesson> dwhLessonsSet = lessons.stream()
                .map(x -> DwhLesson.builder()
                        .name(x.getName())
                        .fullPostText(x.getFullPostText())
                        .description(x.getDescription())
                        .build())
                .collect(Collectors.toSet());

        DwhCourse dwhCourse = DwhCourse.builder()
                .name(course.getName())
                .description(course.getDescription())
                .lessons(dwhLessonsSet)
                .build();

        dwhCourses.save(dwhCourse);
        log.info("Course {} moved to archive", dwhCourse.getName());

        dwhLessonsSet.forEach(x -> {
            x.setDwhCourse(dwhCourse);
            dwhLessons.save(x);
            log.info("Lesson {} of course {} moved to archive", x.getName(), dwhCourse.getName());
        });

        courses.delete(course);

        model.addAttribute("courses", courses.findAll());
        model.addAttribute("msg", "Курс " + dwhCourse.getName() + " перемещен в архив");
        return "teacher/courses/courseList :: courses";
    }

    /**
     * Перенос курса из архива в активные
     * Переносятся все данные курса, а затем открывается страница для ввода дат курса
     *
     * @param dwhCourseId - id курса из DWH
     */
    @GetMapping("archive/{dwh_course_id}/moveToBasic")
    public String etlMoveCourseToBasic(Model model,
                                         @PathVariable("dwh_course_id") long dwhCourseId) {
        DwhCourse dwhCourse = dwhCourses.findById(dwhCourseId);
        Set <DwhLesson> dwhLessons = dwhCourse.getLessons();

        Set <Lesson> lessons = dwhLessons.stream()
                .map(x -> Lesson.builder()
                            .name(x.getName())
                            .description(x.getDescription())
                            .fullPostText(x.getFullPostText())
                            .build())
                .collect(Collectors.toSet());

        UUID uuid = UUID.randomUUID();

        Course course = Course.builder()
                .name(dwhCourse.getName())
                .description(dwhCourse.getDescription())
                .dateBegin(new Date())
                .dateEnd(new Date())
                .uniqueCode(uuid.toString())
                .lessons(lessons)
                .isActive(false) //по умолчанию курс неактивен
                .build();

        this.courses.save(course);
        log.info("Course {} moved from archive to basic", course.getName());

        lessons.forEach(x -> {
            x.setCourse(course);
            x.setIsActive(false); //по умолчанию уроки неактивны
            this.lessons.save(x);
            log.info("Lesson {} of course {} moved from archive to basic", x.getName(), course.getName());
        });

        dwhCourses.delete(dwhCourse);

        model.addAttribute("course", course);
        return "teacher/courses/addDates";
    }
}
