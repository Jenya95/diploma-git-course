package sanevich.diploma.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import sanevich.diploma.DAO.basic.GroupDAO;
import sanevich.diploma.DAO.basic.RoleDAO;
import sanevich.diploma.DAO.basic.UserDAO;
import sanevich.diploma.model.basic.Group;
import sanevich.diploma.model.basic.User;
import sanevich.diploma.service.GitService;
import sanevich.diploma.service.EmailService;

import javax.validation.Valid;
import java.util.HashSet;
import java.util.UUID;

@Controller
@RequestMapping("/")
@PreAuthorize("hasRole('ADMIN')")
public class UsersAdminController {

    private final UserDAO users;
    private final GroupDAO groups;
    private final RoleDAO roles;
    private final Logger log = LoggerFactory.getLogger(this.getClass());
    private final GitService gitService;
    private final EmailService emailService;


    @Autowired
    public UsersAdminController(UserDAO users, GroupDAO groups, RoleDAO roles, GitService gitService,
                                EmailService emailService) {
        this.users = users;
        this.groups = groups;
        this.roles = roles;
        this.gitService = gitService;
        this.emailService = emailService;
    }

    /**
     * Отображение списка всех студентов
     */
    @GetMapping("/users")
    public String showAllUsers(Model model) {
        model.addAttribute("users", users.findAll());
        model.addAttribute("groups", groups.findAll());
        return "teacher/students/userList";
    }

    /**
     * Обработчик удаления студента из системы
     *
     * @param id - id студента для удаления
     */
    @PostMapping("/users/{id}/delete")
    public String deleteUser(@PathVariable("id") long id) {
        log.info("User {} deleted from system", users.findById(id).getEmail());
        users.delete(id);
        return "teacher/students/userList";
    }

    /**
     * Обработчик обновления данных студента в системе
     *
     * @param id - id студента для обновления
     */
    @PostMapping("/users/update")
    public String updateUser(@RequestParam("ID") long id,
                             @RequestParam("email") String email,
                             @RequestParam("password") String pass,
                             @RequestParam("firstName") String firstName,
                             @RequestParam("lastName") String lastName,
                             @RequestParam("groupName") String group,
                             @RequestParam(value = "age", required = false) String age,
                             Model model) {
        User user = users.findById(id);
        user.setPassword(pass);
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setGroup(groups.findByName(group));
        user.setEmail(email);
        if (!age.equals("")) {
            user.setAge(Integer.parseInt(age));
        }
        users.updateUser(user);
        log.info("User {} updated by admin", user.getEmail());
        model.addAttribute("users", users.findAll());
        return "redirect:/users";
    }

    /**
     * Отображение страницы обновления студента
     *
     * @param id - id студента для обновления
     */
    @GetMapping("/users/{id}/update")
    public String updateUser(@PathVariable("id") long id, Model model) {
        model.addAttribute("user", users.findById(id));
        model.addAttribute("groups", groups.findAll());
        return "teacher/students/userUpdate";
    }

    /**
     * Отображение страницы добавления студента
     */
    @GetMapping("/users/addUser")
    public String addUser(Model model) {
        model.addAttribute("groups", groups.findAll());
        model.addAttribute("user", new User());
        return "teacher/students/userAdd";
    }

    /**
     * Отображение страницы добавления группы
     */
    @GetMapping("/users/addGroup")
    public String addGroup(Model model) {
        model.addAttribute("group", new Group());
        return "teacher/groups/groupAdd";
    }

    /**
     * Обработчик добавления группы
     *
     * @param group - объект новой группы
     */
    @PostMapping("/group")
    public String addGroup(@Valid Group group, BindingResult bindingResult, Model model) {
        if (bindingResult.hasErrors()) {
            return addGroup(model);
        }
        groups.save(group);
        log.info("Group {} added by admin", group.getName());
        model.addAttribute("users", users.findAll());
        return "redirect:/users";
    }

    /**
     * Отображение страницы обновления группы
     */
    @GetMapping("/group/{id}/update")
    public String updateGroup(@PathVariable("id") long id, Model model) {
        model.addAttribute("group", groups.findById(id));
        return "teacher/groups/groupUpdate";
    }

    /**
     * Обработчик обновления группы
     *
     * @param name -  новое имя группы
     * @param id   -  id группы для обновления
     */
    @PostMapping("/group/update")
    public String updateGroup(@RequestParam("name") String name,
                              @RequestParam("id") long id,
                              Model model) {
        Group group = groups.findById(id);
        String oldName = group.getName();
        group.setName(name);
        groups.update(group);
        log.info("Group name changed {} -> {}", oldName, name);
        model.addAttribute("users", users.findAll());
        return "redirect:/users";
    }

    /**
     * Обработчик добавления студента
     *
     * @param user - объект нового студента
     */
    @PostMapping("/users")
    public String addUser(@Valid @ModelAttribute User user, BindingResult bindingResult, Model model, RedirectAttributes redirectAttributes) {
        if (bindingResult.hasErrors()) {
            return addUser(model);
        }

        if (users.findByEmail(user.getEmail()) != null) {
            redirectAttributes.addFlashAttribute("msg", "Студент с таким email: " + user.getEmail() + " уже есть в " +
                    "системе");
            return "redirect:/users/addUser";
        }

        Group group = groups.findByName(user.getGroupName());
        user.setGroup(group);
        user.setRoles(new HashSet<>());
        user.addRole(roles.findByName("ROLE_STUDENT"));
        UUID uuid = UUID.randomUUID();
        user.setUniqueCode(uuid.toString().replace("-", ""));
        user.setIsActive(false);
        users.save(user);

        log.info("User with email {} registered by admin, but not activated", user.getEmail());

        String baseUrl = gitService.getBaseEnvLinkURL();

        new Thread(() -> {
            String link;
            String text;
            link = baseUrl + "/activate/" + user.getId() + "/" + user.getUniqueCode();
            text = "Dear " + user.getEmail() + ",\n\nPlease follow this link to confirm your e-mail:\n\n" + link;
            emailService.sendSimpleMessage(user.getEmail(), "Email verification", text);
            log.info("Invite email sent to {}", user.getEmail());
        }).start();

        model.addAttribute("users", users.findAll());
        return "redirect:/users";
    }
}
