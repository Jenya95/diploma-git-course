package sanevich.diploma.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/teacher")
@PreAuthorize("hasRole('ADMIN')")
public class AdminController {

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    /**
     * Отображение старторой страницы для преподавателя
     */
    @GetMapping
    public String showAdminStartPage() {
        log.info("admin page shown");
        return "teacher/teacher";
    }

}
