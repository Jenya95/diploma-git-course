package sanevich.diploma.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import sanevich.diploma.DAO.basic.CourseDAO;
import sanevich.diploma.DAO.basic.RoleDAO;
import sanevich.diploma.DAO.basic.UserDAO;
import sanevich.diploma.model.basic.Course;
import sanevich.diploma.model.basic.User;
import sanevich.diploma.service.GitService;
import sanevich.diploma.service.EmailService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.HashSet;
import java.util.UUID;

@Controller
@RequestMapping("/")
public class InviteController {

    private final UserDAO users;
    private final CourseDAO courses;
    private final RoleDAO roles;
    private final EmailService emailService;
    private final GitService gitService;
    private final Logger log = LoggerFactory.getLogger(this.getClass());


    @Autowired
    public InviteController(UserDAO users, CourseDAO courses, RoleDAO roles, EmailService emailService, GitService
            gitService) {
        this.users = users;
        this.courses = courses;
        this.roles = roles;
        this.emailService = emailService;
        this.gitService = gitService;
    }

    /**
     * Обработчик перехода по инвайт ссылке
     * Сохраняем код курса в атрибутах сессии для того чтобы в дальнейшем добавить его к списку курсов данного
     * польователя (см. CommonService.addCourseIfNotExists())
     *
     * @param uniqueCourseCode - уникальный код курса
     */
    @GetMapping("invite/{course_id}")
    public String inviteForCourse(RedirectAttributes redirectAttributes, HttpServletRequest request, @PathVariable("course_id") String
            uniqueCourseCode) {
        request.getSession().setAttribute("uniqueCourseCode", uniqueCourseCode);

        log.info("UniqueCourseCode {} saved to session attributes", uniqueCourseCode);

        Course course = courses.findByUniqueCode(uniqueCourseCode);
        redirectAttributes.addFlashAttribute("msg", "Курс " + course.getName() + " будет доступен после авторизации");

        return "redirect:/login";
    }

    /**
     * Обработчик регистрации нового пользователя
     */
    @PostMapping("register")
    public String registerUser(RedirectAttributes redirectAttributes, HttpServletRequest request, HttpSession session) {
        String userName = request.getParameter("username");
        String password = request.getParameter("password");
        String msg;

        if (users.findByEmail(userName) != null) {
            msg = "Уважаемый " + userName + "!<br>Вы уже зарегестрированы в системе, можете заходить!";
            redirectAttributes.addFlashAttribute("msg",msg);
            return "redirect:/login";
        }

        User user = new User();
        user.setEmail(userName);
        user.setPassword(password);
        user.setRoles(new HashSet<>());
        user.addRole(roles.findByName("ROLE_STUDENT"));
        user.setIsActive(false);
        UUID uuid = UUID.randomUUID();
        user.setUniqueCode(uuid.toString().replace("-", ""));
        users.save(user);

        log.info("User with email {} registered, but not activated", user.getEmail());

        String baseUrl = gitService.getBaseEnvLinkURL();

        new Thread(() -> {
            String link;
            String text;
            link = baseUrl + "/activate/" + user.getId() + "/" + user.getUniqueCode();
            text = "Dear " + userName + ",\n\nPlease follow this link to confirm your e-mail:\n\n" + link;
            emailService.sendSimpleMessage(userName, "Email verification", text);
            log.info("Invite email sent to {}", userName);
        }).start();

        Object uniqueCourseCode = session.getAttribute("uniqueCourseCode");
        gitService.addCourseIfNotExists(user, uniqueCourseCode, session);

        return "redirect:/registrationInProgress";
    }

    /**
     * Отображение страницы что регистрация в процессе
     */
    @GetMapping("registrationInProgress")
    public String showRegistrationInProgress(Model model) {
        model.addAttribute("msg", "Ссылка для подтверждения отправлена на email");
        return "login";
    }

    /**
     * Обработчик перехода по ссылке из письма
     * @param userId - id студента
     * @param uniqueCode - уникальный код студента
     */
    @GetMapping("activate/{user_id}/{unique_code}")
    public String activateUser(RedirectAttributes redirectAttributes,
                               @PathVariable("user_id") long userId,
                               @PathVariable("unique_code") String uniqueCode) {

        User user = users.findById(userId);
        String msg;

        if (user.getIsActive()) {
            msg = "Уважаемый " + user.getEmail() + "! Вы уже активны в системе, можете заходить!";
        } else if (user.getUniqueCode().equals(uniqueCode)) {
            user.setIsActive(true);
            users.save(user);
            msg = "Уважаемый " + user.getEmail() + "! Вы активированы в системе, можете заходить!";
            log.info("User {} is activated", user.getEmail());
        } else {
            msg = "Попытка обмануть систему обнаружена!";
        }

        redirectAttributes.addFlashAttribute("msg",msg);
        return "redirect:/login";
    }
}
