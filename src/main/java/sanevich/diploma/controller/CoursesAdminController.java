package sanevich.diploma.controller;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import sanevich.diploma.DAO.basic.CourseDAO;
import sanevich.diploma.model.basic.Course;
import sanevich.diploma.service.GitService;

import javax.validation.Valid;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

@Controller
@RequestMapping("/")
@PreAuthorize("hasRole('ADMIN')")
public class CoursesAdminController {

    private final CourseDAO courses;
    private final Logger log = LoggerFactory.getLogger(this.getClass());
    private final GitService gitService;

    @Autowired
    public CoursesAdminController(CourseDAO courses, GitService gitService) {
        this.courses = courses;
        this.gitService = gitService;
    }

    /**
     * Страница со списком всех курсов
     */
    @GetMapping("/courses")
    public String showAllCourses(Model model) {
        model.addAttribute("courses", courses.findAll());
        return "teacher/courses/courseList";
    }

    /**
     * Страница с полями для ввода дат при переносе курса из архива в активные
     */
    @GetMapping("/addDatesForArchive")
    public String addDatesForArchive(Model model) {
        model.addAttribute("courses", courses.findAll());
        return "teacher/courses/addDates";
    }

    /**
     * Страница для добавления курса
     */
    @GetMapping("/courses/addCourse")
    public String addCourse(Model model) {
        if (!model.containsAttribute("course")) {
            model.addAttribute("course", new Course());
        }
        return "teacher/courses/courseAdd";
    }

    /**
     * Обработка добавления нового курса
     *
     * @param course - объект курса с заполненными полями
     */
    @PostMapping("/courses")
    public String addCourse(@Valid Course course, BindingResult bindingResult, Model model, RedirectAttributes redirectAttributes) {
        if (bindingResult.hasErrors()) {
            return addCourse(model);
        }

        if (course.getDateBegin() == null || (course.getDateEnd() == null)) {
            redirectAttributes.addFlashAttribute("msg", "Заполните все даты!");
            redirectAttributes.addFlashAttribute("course", course);
            return "redirect:/courses/addCourse";
        }

        UUID uuid = UUID.randomUUID();
        course.setUniqueCode(uuid.toString());
        course.setIsActive(true);
        courses.save(course);
        log.info("Course {} saved", course.getName());
        model.addAttribute("courses", courses.findAll());
        return "redirect:/courses";
    }

    /**
     * Обработка активирования курса AJAX
     *
     * @param courseId - id курса
     * @param checked  - требуемое состояние
     */
    @GetMapping("/courses/{course_id}/updateActive")
    public String updateActive(Model model,
                               @PathVariable("course_id") long courseId,
                               @RequestParam(value = "checked", required = false)
                                       Boolean checked) {

        Course course = courses.findById(courseId);
        if (checked == null) {
            checked = false;
        }
        course.setIsActive(checked);
        courses.save(course);
        if (course.getIsActive()) {
            log.info("Course {} activated", course.getName());
        } else {
            log.info("Course {} deactivated", course.getName());
        }
        model.addAttribute("courses", courses.findAll());

        return "teacher/courses/courseList :: courses";
    }

    /**
     * Обработка обновления даты действия курса
     *
     * @param courseId  - id курса
     * @param dateBegin - новая дата начала курса
     * @param dateEnd   - новая дата окончания курса
     */
    @PostMapping("/courses/{course_id}/updateDates")
    public String updateDates(RedirectAttributes redirectAttributes,
                              @PathVariable("course_id") long courseId,
                              @RequestParam(value = "dateBegin", required = false) String dateBegin,
                              @RequestParam(value = "dateEnd", required = false) String dateEnd) throws ParseException {
        Course course = courses.findById(courseId);

        log.info("Old dates of course {} : {} - {}", course.getName(), course.getDateBegin(), course.getDateEnd());

        SimpleDateFormat formatter = new SimpleDateFormat("dd.MM.yyyy");
        course.setDateBegin(formatter.parse(dateBegin));
        course.setDateEnd(formatter.parse(dateEnd));
        courses.save(course);

        log.info("New dates of course {} : {} - {}", course.getName(), course.getDateBegin(), course.getDateEnd());

        redirectAttributes.addFlashAttribute("courses", courses.findAll());

        return "redirect:/course/{course_id}";
    }

    /**
     * Обработка обновления ссылки на курс git
     *
     * @param courseId - id курса
     * @param gitLink  - ссылка на новый git проект
     */
    @PostMapping("/courses/{course_id}/updateGitLink")
    public String updateGitLink(RedirectAttributes redirectAttributes,
                                @PathVariable("course_id") long courseId,
                                @RequestParam(value = "gitLink", required = false) String gitLink) {
        Course course = courses.findById(courseId);
        log.info("Old git link of course {} : {}", course.getName(), course.getOriginLink());

        try {
            String origin = gitService.findOriginByLink(gitLink);
            if (origin != null) {
                log.info("New git link of course {} : {}", course.getName(), gitLink);
                course.setOriginLink(gitLink);
                course.setOrigin(origin);
                courses.save(course);
            } else {
                redirectAttributes.addFlashAttribute("msg", "Проект " + gitLink + " в Git не существует!");
            }
        } catch (Exception e) {
            log.warn("GitLab connection error");
            redirectAttributes.addFlashAttribute("msg", "Проект " + gitLink + "в Git не существует!");
        }

        redirectAttributes.addFlashAttribute("courses", courses.findAll());
        return "redirect:/course/{course_id}";
    }

    /**
     * Обработка добавления даты при переносе курса из архива в активные
     *
     * @param courseId  - id курса
     * @param dateBegin - дата начала курса
     * @param dateEnd   - дата окончания курса
     */
    @PostMapping("/courses/{course_id}/updateDatesForArchive")
    public String updateDatesForArchive(RedirectAttributes redirectAttributes,
                                        @PathVariable("course_id") long courseId,
                                        @RequestParam(value = "dateBegin", required = false) String dateBegin,
                                        @RequestParam(value = "dateEnd", required = false) String dateEnd) throws ParseException {
        Course course = courses.findById(courseId);
        SimpleDateFormat formatter = new SimpleDateFormat("dd.MM.yyyy");

        if (dateBegin.equals("") || (dateEnd.equals(""))) {
            if (!dateBegin.equals("")) {
                course.setDateBegin(formatter.parse(dateBegin));
                courses.save(course);
            }
            if (!dateEnd.equals("")) {
                course.setDateEnd(formatter.parse(dateEnd));
                courses.save(course);
            }

            redirectAttributes.addFlashAttribute("msg", "Заполните все даты!");
            redirectAttributes.addFlashAttribute("course", course);
            return "redirect:/addDatesForArchive";
        }

        Date dateBeginLocal = formatter.parse(dateBegin);
        Date dateEndLocal = formatter.parse(dateEnd);

        if (dateBeginLocal.after(dateEndLocal)) {
            redirectAttributes.addFlashAttribute("msg", "Дата окончания курса не может быть меньше даты начала!");
            redirectAttributes.addFlashAttribute("course", course);
            return "redirect:/addDatesForArchive";
        }

        course.setDateBegin(dateBeginLocal);
        course.setDateEnd(dateEndLocal);
        courses.save(course);

        log.info("Dates of course {} : {} - {}", course.getName(), course.getDateBegin(), course.getDateEnd());

        redirectAttributes.addFlashAttribute("courses", courses.findAll());

        return "redirect:/courses";
    }
}
