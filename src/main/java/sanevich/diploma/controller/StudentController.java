package sanevich.diploma.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.method.annotation.MvcUriComponentsBuilder;
import sanevich.diploma.DAO.basic.CourseDAO;
import sanevich.diploma.DAO.basic.GroupDAO;
import sanevich.diploma.DAO.basic.LessonDAO;
import sanevich.diploma.DAO.basic.UserDAO;
import sanevich.diploma.model.basic.Course;
import sanevich.diploma.model.basic.Lesson;
import sanevich.diploma.model.basic.User;
import sanevich.diploma.service.GitService;
import sanevich.diploma.storage.StorageService;

import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.stream.Collectors;


@Controller
@RequestMapping("/student")
@PreAuthorize("hasAnyRole('STUDENT','ADMIN')")
public class StudentController {

    private final CourseDAO courses;
    private final UserDAO users;
    private final LessonDAO lessons;
    private final GroupDAO groups;
    private final StorageService storageService;
    private final GitService gitService;
    private final Logger log = LoggerFactory.getLogger(this.getClass());


    @Autowired
    public StudentController(CourseDAO courses, UserDAO users, LessonDAO lessons, GroupDAO groups, StorageService
            storageService, GitService gitService) {
        this.courses = courses;
        this.users = users;
        this.lessons = lessons;
        this.groups = groups;
        this.storageService = storageService;
        this.gitService = gitService;
    }

    @Cacheable("availableCourses")
    public List<Course> getAvailableCourses() {
        User student = getCurrentUser();
        return courses.findByUser(users.findById(student.getId()))
                .stream()
                .filter(Course::getIsActive)
                .collect(Collectors.toList());
    }

    public User getCurrentUser() {
        UserDetails user = (org.springframework.security.core.userdetails.User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return users.findByEmail(user.getUsername());
    }

    /**
     * Отображение страницы со списоком курсов данного студента
     */
    @GetMapping
    public String showStudentStartPage(HttpSession session, Model model) {

        User user = getCurrentUser();

        //если студент прошел по инвайт ссылке курса, то она останется в атрибуте сессии и после авторизации
        //курс добавится к списку доступных курсов

        Object uniqueCourseCode = session.getAttribute("uniqueCourseCode");
        gitService.addCourseIfNotExists(user, uniqueCourseCode, session);

        List<Course> availableCourses = getAvailableCourses();
        model.addAttribute("courses", availableCourses);
        return "student/student";
    }

    /**
     * Отображение страницы со списоком лекций данного курса для студента
     *
     * @param id - id курса
     */
    @GetMapping("/{id}")
    public String showListOfLessonsInCourse(@PathVariable("id") long id, Model model) {
        List<Course> availableCourses = getAvailableCourses();
        //нельзя смотреть лекции не своих курсов!
        Course course = courses.findById(id);
        if (availableCourses.contains(course)) {
            List<Lesson> availableLessons = lessons.findByCourse(course)
                    .stream()
                    .filter(Lesson::getIsActive)
                    .collect(Collectors.toList());

            model.addAttribute("lessons", availableLessons);
            model.addAttribute("course", course);
            model.addAttribute("files", storageService.loadByCourse(course)
                    .map(path ->
                            MvcUriComponentsBuilder.fromMethodName(LessonsAdminController.class,
                                    "serveFile",
                                    path.getFileName().toString()).build().toString())
                    .collect(Collectors.toList()));
            log.info("User {} opened course {}", getCurrentUser().getEmail(), course.getName());

        }
        return "student/lessonListStudent";
    }

    /**
     * Отображение страницы лекцией для студента
     *
     * @param courseId - id курса этой лекции
     * @param lessonId - id лекции
     */
    @GetMapping("/{course_id}/{lesson_id}")
    public String showLessonData(@PathVariable("course_id") long courseId,
                                 @PathVariable("lesson_id") long lessonId, Model model) {
        List<Course> availableCourses = getAvailableCourses();
        //нельзя смотреть лекции не своих курсов!
        if (availableCourses.contains(courses.findById(courseId))) {
            model.addAttribute("lesson", lessons.findById(lessonId));
            model.addAttribute("course", courses.findById(courseId));

            log.info("User {} opened lesson {} of course {}",
                    getCurrentUser().getEmail(),
                    lessons.findById(lessonId),
                    courses.findById(courseId).getName());
        }
        return "student/lesson";
    }

    /**
     * Отображение страницы редактирования информации о себе для студента
     */
    @GetMapping("/editProfile")
    public String editProfile(Model model) {
        User user = getCurrentUser();
        model.addAttribute("user", user);
        model.addAttribute("groups", groups.findAll());
        return "student/editProfile";
    }

    /**
     * Обработка обновления информации о студенте - Имя, Фамилия, Возраст, Группа
     * Нельзя апдейтить не своего юзера
     */
    @PostMapping("/editProfile/{user_id}/update")
    public String updateProfile(@PathVariable("user_id") long userId,
                                @RequestParam("firstName") String firstName,
                                @RequestParam("lastName") String lastName,
                                @RequestParam("groupName") String groupName) {

        User user = users.findById(userId);
        String oldFirstName = user.getFirstName();
        String oldLastName = user.getLastName();
        String oldGroup = user.getGroup() != null ? user.getGroup().getName() : null;

        if (getCurrentUser().equals(user)) {
            user.setFirstName(firstName);
            user.setLastName(lastName);
            user.setGroup(groups.findByName(groupName));
            users.save(user);
            log.info("User {} updated by himself:\nFirst name {} -> {}\nLast name {} -> {}\nGroup {} -> {}",
                    user.getEmail(),
                    oldFirstName,
                    user.getFirstName(),
                    oldLastName,
                    user.getLastName(),
                    oldGroup,
                    user.getGroup().getName());
        }

        return "redirect:/student";
    }

}
