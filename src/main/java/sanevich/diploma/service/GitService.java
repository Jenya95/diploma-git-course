package sanevich.diploma.service;

import org.apache.commons.lang3.RandomStringUtils;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.mvc.method.annotation.MvcUriComponentsBuilder;
import sanevich.diploma.controller.SecurityController;
import sanevich.diploma.DAO.basic.CourseDAO;
import sanevich.diploma.DAO.basic.UserDAO;
import sanevich.diploma.model.basic.Course;
import sanevich.diploma.model.basic.User;
import sanevich.diploma.model.gitlab.*;
import sanevich.diploma.model.gitlab.response.Project;

import javax.servlet.http.HttpSession;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

@Service
@PropertySource("classpath:gitlab.properties")
public class GitService {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    private final EmailService emailService;
    private final RestTemplate restTemplate = new RestTemplate();
    private final String GITLAB_ADMIN_TOKEN;
    private final String GITLAB_BASE_URL;
    private final String GITLAB_ADMIN_ID;
    private final UserDAO users;
    private final CourseDAO courses;


    @Autowired
    public GitService(EmailService emailService, Environment env, UserDAO users, CourseDAO courses) {
        this.emailService = emailService;
        GITLAB_ADMIN_TOKEN = env.getProperty("gitlab.admin.token");
        GITLAB_BASE_URL = env.getProperty("gitlab.base");
        GITLAB_ADMIN_ID = env.getProperty("gitlab.admin.id");
        this.users = users;
        this.courses = courses;
    }


    @Cacheable("urls")
    public String getBaseEnvLinkURL() {
        String baseUrl = MvcUriComponentsBuilder.fromMethodName(SecurityController.class,
                "makeBaseUrl").build().toString();
        return baseUrl.substring(0, baseUrl.length() - 1);
    }

    public void addCourseIfNotExists(User user, Object uniqueCourseCode, HttpSession session) {
        if (uniqueCourseCode != null) {
            if (user.getCourses() == null) {
                user.setCourses(new HashSet<>());
            }
            Course course = courses.findByUniqueCode(uniqueCourseCode.toString());
            if (!user.getCourses().contains(course)) {
                user.addCourse(course);
                users.save(user);
                new Thread(() -> {
                    try {
                        connectUserWithCourseInGit(user, course);
                        logger.info("Course {} added to {}", course.getName(), user.getEmail());
                    } catch (Exception e) {
                        logger.error("Git is not available");
                    }
                }).start();
            }
            session.setAttribute("uniqueCourseCode", null);
        }
    }

    private void connectUserWithCourseInGit(User user, Course course) {
        addUserToGitLab(user);
        long forkedProjectId = forkProject(user, course);
        editProject(user, forkedProjectId);
        reduceUserAccessRightsToDeveloper(user, forkedProjectId);
        addTeacherToForkedProject(forkedProjectId);
    }

    void addUserToGitLab(User user) {
        HttpHeaders headers = new HttpHeaders();
        headers.set("Accept", MediaType.APPLICATION_JSON_VALUE);
        headers.set("PRIVATE-TOKEN", GITLAB_ADMIN_TOKEN);

        String url = GITLAB_BASE_URL + "/api/v3/users";
        logger.info("Step: Check if user already exists in gitlab");
        logger.info("Send GET request to {} ...", url);
        ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.GET, new HttpEntity(headers),
                String.class);
        logger.info("Response: {}", response.getBody());

        if (!response.getBody().contains(user.getEmail())) {
            String pwd = user.getPassword().length() > 8 ? user.getPassword() : getSomePassword();
            String username = user.getEmail().substring(0, user.getEmail().indexOf("@"));

            RequestAddUser reqBody = RequestAddUser.builder()
                    .email(user.getEmail())
                    .password(pwd)
                    .username(username)
                    .name(username)
                    .projectsLimit(1)
                    .canCreateGroup(false)
                    .build();

            HttpEntity<RequestAddUser> request = new HttpEntity<>(reqBody, headers);

            logger.info("Step: Registration of user {}", user.getEmail());
            logger.info("Send POST request to {} ...", url);
            logger.info("Body of POST: {}", reqBody);
            ResponseEntity<String> httpPostResponse = restTemplate.postForEntity(url, request, String.class);

            try {
                JSONObject obj = new JSONObject(httpPostResponse.getBody());
                user.setGitId(obj.getLong("id"));
            } catch (JSONException e) {
                e.printStackTrace();
            }

            logger.info("User {} was added to GitLab, response {}", user.getEmail(), httpPostResponse.getBody());

            new Thread(() -> {
                String text;
                text = "Dear " + user.getEmail() + "!,\n\nYou have been registered in GitLab:\n\n" + "Login :" + user
                        .getEmail() + "\nPassword: " + pwd;
                emailService.sendSimpleMessage(user.getEmail(), "GitLab registration", text);
            }).start();

            RequestOAuth auth = RequestOAuth.builder()
                    .grantType("password")
                    .password(pwd)
                    .username(username)
                    .build();

            HttpEntity<RequestOAuth> reqOAuth = new HttpEntity<>(auth);

            String urlAuth = GITLAB_BASE_URL + "/oauth/token";

            logger.info("Step: Authentication of user {}", user.getEmail());
            logger.info("Send POST request to {} ...", urlAuth);
            logger.info("Body of POST: {}", auth);
            ResponseEntity<ResponseOAuth> responseOAuth = restTemplate.postForEntity(urlAuth, reqOAuth,
                    ResponseOAuth.class);
            logger.info("User authenticated, response {}", responseOAuth.getBody());

            user.setGitAccessToken(responseOAuth.getBody().getAccessToken());
            user.setGitRefreshToken(responseOAuth.getBody().getRefreshToken());
            this.users.save(user);
        }
    }

    long forkProject(User user, Course course) {
        String token = user.getGitAccessToken();
        HttpHeaders headers = new HttpHeaders();
        headers.set("Accept", MediaType.APPLICATION_JSON_VALUE);
        headers.set("Authorization", "Bearer " + token);

        HttpEntity<String> requestFork = new HttpEntity<>(headers);

        String urlFork = GITLAB_BASE_URL + "/api/v3/projects/fork/";
        logger.info("Step: Fork project for user");
        logger.info("Send POST request to {} ...", urlFork + course.getOrigin());
        logger.info("Headers of POST: {}", requestFork.getHeaders());

        if (course.getOrigin() == null) {
            setCourseOriginByLink(course);
        }

        ResponseEntity<String> responseFork =
                restTemplate.postForEntity(urlFork + course.getOrigin(),
                        requestFork, String.class);

        logger.info("Git project {} was forked by user {}, response: {}",
                course.getName(),
                user.getEmail(),
                responseFork.getBody());

        try {
            JSONObject obj = new JSONObject(responseFork.getBody());
            return obj.getLong("id");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return 0;
    }

    private void setCourseOriginByLink(Course course) {
        String originLink = course.getOriginLink();
        String origin = findOriginByLink(originLink);
        course.setOrigin(origin);
        courses.save(course);
    }

    public String findOriginByLink(String originLink) {
        HttpHeaders originHeaders = new HttpHeaders();
        originHeaders.set("Accept", MediaType.APPLICATION_JSON_VALUE);
        originHeaders.set("PRIVATE-TOKEN", GITLAB_ADMIN_TOKEN);

        String[] urlParts = originLink.split("/");

        String urlProjectOrigin = GITLAB_BASE_URL + "/api/v3/projects?search=" + urlParts[urlParts.length - 1];
        logger.info("Step: get origin of project by link");
        logger.info("Send GET request to {} ...", urlProjectOrigin);
        ResponseEntity<Project[]> response = restTemplate.exchange(urlProjectOrigin, HttpMethod.GET, new HttpEntity
                (originHeaders), Project[].class);
        logger.info("Response: {}", (Object[]) response.getBody());

        return Arrays.stream(response.getBody())
                .filter(x -> x.getOwner().getUsername().equals(urlParts[urlParts.length - 2]))
                .findFirst()
                .get().getId().toString();
    }

    private String getSomePassword() {
        String CHARACTERS = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        return RandomStringUtils.random(10, CHARACTERS);
    }

    void editProject(User user, long forkedProjectId) {
        String token = user.getGitAccessToken();
        HttpHeaders headers = new HttpHeaders();
        headers.set("Accept", MediaType.APPLICATION_JSON_VALUE);
        headers.set("Authorization", "Bearer " + token);

        RequestEditProject body = RequestEditProject.builder()
                .id(forkedProjectId)
                .issuesEnabled(false)
                .mergeRequestsEnabled(false)
                .visibilityLevel(0)
                .build();

        HttpEntity<RequestEditProject> reqEditProject = new HttpEntity<>(body, headers);

        String urlProjects = GITLAB_BASE_URL + "/api/v3/projects/";
        logger.info("Step: Edit permissions for forked project");
        logger.info("Send PUT request to {} ...", urlProjects);
        logger.info("Body of PUT: {}", body);
        restTemplate.put(urlProjects + forkedProjectId, reqEditProject);

        logger.info("Send GET request to {} ...", urlProjects);
        ResponseEntity<String> dataEditedProject = restTemplate.exchange(urlProjects + forkedProjectId, HttpMethod.GET, new HttpEntity<>(headers), String.class);

        logger.info("Project {} for user {} was edited, rights reduced, response: {}",
                forkedProjectId,
                user.getEmail(),
                dataEditedProject.getBody());
    }

    void reduceUserAccessRightsToDeveloper(User user, long forkedProjectId) {
        HttpHeaders headers = new HttpHeaders();
        headers.set("Accept", MediaType.APPLICATION_JSON_VALUE);
        headers.set("PRIVATE-TOKEN", GITLAB_ADMIN_TOKEN);

        RequestEditAccessRights body = RequestEditAccessRights.builder()
                .id(forkedProjectId)
                .userId(user.getGitId())
                .accessLevel(30)
                .build();

        HttpEntity<RequestEditAccessRights> reqReduceAccess = new HttpEntity<>(body, headers);

        String url = GITLAB_BASE_URL + "/api/v3/projects/" + forkedProjectId + "/members/" + user.getGitId();

        logger.info("Step: Reduce access rights for user");
        logger.info("Send PUT request to {} ...", url);
        logger.info("Body of PUT: {}", body);
        restTemplate.put(url, reqReduceAccess);

        logger.info("Access to project {} was reduced for user {}", forkedProjectId, user.getGitId());
    }

    void addTeacherToForkedProject(long forkedProjectId) {
        HttpHeaders headers = new HttpHeaders();
        headers.set("Accept", MediaType.APPLICATION_JSON_VALUE);
        headers.set("PRIVATE-TOKEN", GITLAB_ADMIN_TOKEN);

        RequestEditAccessRights body = RequestEditAccessRights.builder()
                .id(forkedProjectId)
                .userId(Long.valueOf(GITLAB_ADMIN_ID))
                .accessLevel(40)
                .build();

        HttpEntity<RequestEditAccessRights> reqAddAccess = new HttpEntity<>(body, headers);

        String url = GITLAB_BASE_URL + "/api/v3/projects/" + forkedProjectId + "/members";

        logger.info("Step: Add teacher as member to forked project");
        logger.info("Send POST request to {} ...", url);
        logger.info("Body of POST: {}", body);
        ResponseEntity<String> responseAddAccess =
                restTemplate.postForEntity(url, reqAddAccess, String.class);

        logger.info("Access to project {} was granted to teacher, response {}",
                forkedProjectId,
                responseAddAccess.getBody());
    }
}
