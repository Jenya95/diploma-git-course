$( document ).ready(function() {
    console.log( "ready!" );
    dateObj = {
        weekdaysShort: ['ВС', 'ПН', 'ВТ', 'СР', 'ЧТ', 'ПТ', 'СБ'],
        monthsFull: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
        monthsShort: ['Янв', 'Фев', 'Мар', 'Апр', 'Май', 'Июнь', 'Июль', 'Авг', 'Сен', 'Окт', 'Ноя', 'Дек'],
        weekdaysFull: ['Воскресенье', 'Понедельник', 'Вторник', 'Среда', 'Четверг', 'Пятница', 'Суббота'],
        selectMonths: true,
        selectYears: 5,
        today: 'Сегодня',
        clear: 'Очистить',
        close: 'Ok',
        closeOnSelect: false,
        format: 'dd.mm.yyyy'
    };
    $('.collapsible').collapsible();
    $('.datepicker-start').pickadate(dateObj);
    $('.datepicker-end').pickadate(dateObj);

    $('.tooltipped').tooltip({delay: 50});
    
});
